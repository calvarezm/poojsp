package cl.Inacap.FormPersonas.DAO;

import java.util.ArrayList;
import java.util.List;

import cl.Inacap.FormPersonas.DTO.Persona;

public class PersonaDAO {
	
	private static List<Persona> arrPersona = new ArrayList<Persona>();
	
	//Agregar persona a la clase
	public void addPersona(Persona p) {  //Insert 
		arrPersona.add(p);
	}
	
	public List<Persona> getAllPersonas(){ //select *  
		return arrPersona;
	}
	
	
	public Persona getPersonabyIden(int id) {  //Select * where
		return arrPersona.get(id);
	}
	
	public void updatePersona(Persona p,int id) {  //Update 
		arrPersona.set(id, p); 
	}
	
	public void deletePersona (int id) { //
		arrPersona.remove(id);
	}
	
}
