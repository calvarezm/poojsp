package cl.Inacap.FormPersonas.Controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.Inacap.FormPersonas.DAO.PersonaDAO;
import cl.Inacap.FormPersonas.DTO.Persona;


@WebServlet("/EditarPersona.do")
public class EditarPersona extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public EditarPersona() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int indice=Integer.parseInt(request.getParameter("index"));
		
		PersonaDAO dp=new PersonaDAO();
		request.setAttribute("getPersona", dp.getPersonabyIden(indice));
		request.setAttribute("IndexPersona", indice);
		
		
		request.getRequestDispatcher("Site/FormPersona.jsp").forward(request, response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombrePersona=request.getParameter("nombre").toString();
		String rut=request.getParameter("rut").toString();
		int anio = Integer.parseInt( request.getParameter("anio").toString());
		String email=request.getParameter("Email").toString();
		String Direcccion=request.getParameter("Direccion").toString();
		
		int Index=Integer.parseInt(request.getParameter("index"));
       
		
		Persona p =new Persona();
		
		
		p.setNombre(nombrePersona);
		p.setRut(rut);
		p.setAnos(anio);
		p.setEmail(email);
		p.setDireccion(Direcccion);
		
		new PersonaDAO().updatePersona(p, Index);
		
		response.sendRedirect("ListarControllers.do");
		
		
		
		
		
		
	}

}
