package cl.Inacap.FormPersonas.Controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.Inacap.FormPersonas.DAO.PersonaDAO;

/**
 * Servlet implementation class eliminarPersona
 */
@WebServlet("/eliminarPersona.do")
public class eliminarPersona extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public eliminarPersona() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int indice=Integer.parseInt(request.getParameter("IdPersona"));
		
		String NombrePersona=request.getParameter("NombrePersona").toString();
		
		new PersonaDAO().deletePersona(indice);
		
		PrintWriter out=response.getWriter();
		
		out.write("Se Elimino el Usuario: " + NombrePersona + " - " + indice + " de forma correcto");
		
		
	}

}
