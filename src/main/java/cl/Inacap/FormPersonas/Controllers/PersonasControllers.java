package cl.Inacap.FormPersonas.Controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.Inacap.FormPersonas.DAO.PersonaDAO;
import cl.Inacap.FormPersonas.DTO.Persona;

/**
 * Servlet implementation class PersonasControllers
 */
@WebServlet("/PersonasControllers.do")
public class PersonasControllers extends HttpServlet {
	private static final long serialVersionUID = 1L;

    
    public PersonasControllers() {
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		//
		/*
		 * OBS:ACA SE INGRESA UN OBJETO A LA LIST<Persona> "FORMA ANTIGUA " ahora se utliza el DAO.
		 
		Persona p =new Persona();
		p.setNombre("Carlos");
		p.setRut("122122");
		p.setAnos(33);
		p.setEmail("Carlos@inacapmail.cl");
		p.setDireccion("Limache");
		List<Persona> arrPersonas=new ArrayList<Persona>();
		arrPersonas.add(p);
		/*
		 * 
		CRUD
			Create ===> 	
			Read
			Update
			Delete
		
		Persona pa =new Persona("Carlos","1111111","carlos@incapmail.cl",33,"Limache");
		arrPersonas.add(pa);
		*/
		
		String titulo="Formulario de ingreso";
		
		request.setAttribute("tituloPagina",titulo ); 
		
		
		
		request.getRequestDispatcher("Site/FormPersona.jsp").forward(request, response);
		
				
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String nombrePersona=request.getParameter("nombre").toString();
		String rut=request.getParameter("rut").toString();
		int anio = Integer.parseInt( request.getParameter("anio").toString());
		String email=request.getParameter("Email").toString();
		String Direcccion=request.getParameter("Direccion").toString();
       
		
		Persona p =new Persona();
		
		
		p.setNombre(nombrePersona);
		p.setRut(rut);
		p.setAnos(anio);
		p.setEmail(email);
		p.setDireccion(Direcccion);
		
		
		
		new PersonaDAO().addPersona(p);   
		
		/*
		 * PersonaDAO ps=new PersonaDAO();
		ps.addPersona(p);
		*/
		
		response.sendRedirect("Home.do");
		
		
		
	}

}
