package cl.Inacap.FormPersonas.Controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.Inacap.FormPersonas.DAO.PersonaDAO;
import cl.Inacap.FormPersonas.DTO.Persona;

/**
 * Servlet implementation class Home
 */
@WebServlet("/Home.do")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Home() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PersonaDAO pd=new PersonaDAO();
		
		for(int i=1;i<=20;i++) {
			Persona p=new Persona();
			p.setNombre("Persona "+i);
			p.setRut("1111111-"+i);
			p.setAnos(2021);
			p.setEmail("correo"+i+"@correo.cl");
			p.setDireccion("Casa usuario "+i*100);
			
			pd.addPersona(p);
		}
		
		request.setAttribute("Titulo", "Sitio Mantenedor");
		
		request.getRequestDispatcher("Site/Home.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
