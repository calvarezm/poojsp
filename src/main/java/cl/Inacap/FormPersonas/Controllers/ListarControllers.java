package cl.Inacap.FormPersonas.Controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.Inacap.FormPersonas.DAO.PersonaDAO;
import cl.Inacap.FormPersonas.DTO.Persona;

/**
 * Servlet implementation class ListarControllers
 */
@WebServlet("/ListarControllers.do")
public class ListarControllers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListarControllers() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		//PersonaDAO ps=new PersonaDAO();
		//request.setAttribute("ListaPersonas",ps.getAllPersonas());
		
		PersonaDAO ps=new PersonaDAO();
		List<Persona> ListaP=ps.getAllPersonas();
		request.setAttribute("ListaPersonas", ListaP);
		
		/*
		request.setAttribute("ListaPersonas", new PersonaDAO().getAllPersonas());
		*/
		request.getRequestDispatcher("Site/ListarPersonas.jsp").forward(request, response);

	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
