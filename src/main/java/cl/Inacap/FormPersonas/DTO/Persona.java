package cl.Inacap.FormPersonas.DTO;

public class Persona {
	
	/*
	 * Atributos
	 * */
	
	private String nombre;
	private String rut;
	private String email;
	private int anos;
	private String direccion;
	
	/*
	 * 
	 * Metodos jwt---> 
	 * 
	 * */
	
	public Persona(String nombre, String rut, String email, int anos, String direccion) {
		this.nombre = nombre;
		this.rut = rut;
		this.email = email;
		this.anos = anos;
		this.direccion = direccion;
	}
	
	public Persona() {}
	
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getAnos() {
		return anos;
	}
	public void setAnos(int anos) {
		this.anos = anos;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
}
