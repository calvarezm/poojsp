<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Boostrap Ejemplos.</title>
</head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    

    <style>
        .border{
            border:1px solid red  !important;
        }
        .col-centered{
            margin:auto;
        }
    </style>

<body>


    <div class="container  ">
        <div class="row ">

            <div class="col-md-4 col-centered" style="margin-top: 10%;">

                <div class="row">
                    <div class="col-md-12">
                        <h1>Login de usuario</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-10">
                        <label for="CorreoUsuario" id="">Correo Electronico</label>
                        <input type="email" id="CorreoUsuario" class="form-control">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-10">
                        <label for="PasswordUsuario" id="">Password </label>
                        <input type="password" name="" id="PasswordUsuario" class="form-control">
                   </div>
                </div>

                <div class="row" style="margin-top: 20px;">
                    <div class="col-md-10">
                        <button class="btn btn-primary btn-block" onclick="loginUsuario()">Ingresar</button>
                    </div>
                </div>

            </div>
            

           
        </div>

    </div>

    
</body>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>


    <script>

        function loginUsuario(){
            if($("#CorreoUsuario").val()==''  || $("#PasswordUsuario").val()==''){
                alert("Favor de ingresar ambos campos");
                return 0;
            }else{
                var sendData={
                    'CorreoUsuario':  $("#CorreoUsuario").val(),
                    'PasswordUsuario': $("#PasswordUsuario").val()

                };
                $.ajax({
                    type:"POST",
                    url:"Login.do",
                    data:sendData,
                    success:function (obj){
                        alert(obj);


                    }

                });
            }

        }

    </script>

</html>