<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Responsive</title>
</head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

<style>
    .border{
       border: 1px solid #000 !important;   
       min-height:20px;
       margin-bottom: 20px;
    }
  
    .col-centered{
        float: none;
        margin: 0 auto;
    }

</style> 
<body>

    <div class="container" style="margin-top: 20px;">

        <div class="row">
            <div class="col-6 col-centered text-center">
            	
            	<div class="row">
            		<div class="col-md-12">
            			 <h1>Formulario de persona</h1>
            		</div>
            		
            		<div class="col-md-6">
            			<a class="btn btn-primary btn-block" href="PersonasControllers.do">Ingresar Persona</a>
            		</div>
            		
            		<div class="col-md-6">
            			<a class="btn btn-primary btn-block" href="ListarControllers.do">Listar Personas</a>
            		</div>
            		
            	</div>
               
            </div>
        </div>

       
    </div>



    
</body>



<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>


</html>