<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@page import="java.util.List" %>   
<%@page import="cl.Inacap.FormPersonas.DAO.PersonaDAO" %> 
<%@page import="cl.Inacap.FormPersonas.DTO.Persona"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>

    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Responsive</title>
</head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<link rel="stylesheet" href="Site/css/font-awesome.min.css">
<link rel="stylesheet" href="Site/css/jquery.dataTables.min.css">

<style>
    .border{
       border: 1px solid #000 !important;   
       min-height:20px;
       margin-bottom: 20px;
    }
  
    .col-centered{
        float: none;
        margin: 0 auto;
    }

</style> 
<body>

    <div class="container" style="margin-top: 20px;">

        <div class="row">
            <div class="col-10 col-centered text-center">
            	
            	<div class="row">
            		<div class="col-md-12">
            			 <h1>Listar Personas</h1>
            		</div>
            		
            		<div class="col-md-12">

                        <table class="table table-bordered table-striped" id="tabla-trabajo">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Rut</th>
                                    <th>Años</th>
                                    <th>Email</th>
                                    <th>Dirección</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                               
                            </thead>
                            

                            <tbody>
                                                        
                            	<c:forEach items="${ListaPersonas }"  var="p" varStatus="recorrido"> 
                            	
                            		<tr>
                            			<td>${p.nombre }</td>
                            			<td>${p.rut }</td>
                            			<td>${p.anos}</td>
                            			<td>${p.email }</td>
                            			<td>${p.direccion } </td>
                            			
                            			<td><a href="EditarPersona.do?index=${recorrido.index}" class="btn btn-success btn-sm" ${recorrido.index }><i class="fa fa-edit"></a></td>
                                    	
                                    	<td>
                                    		<button class="btn btn-danger btn-sm" type="button" onclick="eliminarPersona(${recorrido.index} ,'${p.nombre }')">
                                    			<i class="fa fa-trash"></i>
                                    		</button>
                                    	</td>
                                    </tr>
                            	
                            	</c:forEach>
                            	
                            	
                            
                            
                            	<%
                            	
                            	/*
                            	//List<Persona> p =(List<Persona>) request.getAttribute("ListaPersonas"); 
                            		
                            	List<Persona> p = new ArrayList<Persona>();
                            	
                            	
                            	for(int i=0;i<p.size();i++){
                            		
                            	%>
                            
                                <tr>
                                    <td><%=p.get(i).getNombre() %></td>
                                    <td><%=p.get(i).getRut()%></td>
                                    <td><%=p.get(i).getAnos()%></td>
                                    <td><%=p.get(i).getEmail()%></td>
                                    <td><%=p.get(i).getDireccion()%></td>

                                    <td><button class="btn btn-success btn-sm"><i class="fa fa-edit fa-spin"></button></td>
                                    <td><button class="btn btn-danger btn-sm"><i class="fa fa-trash"></button></td>
                                </tr>
                                
                                <%
                            	}
                            	*/
                            	
                                %>
                                
                                
                               
                               




                            </tbody>

                        </table>
            			
            		</div>
            		
            	</div>
               
            </div>
        </div>

       
    </div>



    
</body>



<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
<script src="Site/js/jquery.dataTables.js"></script>


<script>
    $(document).ready(function (){
        $("#tabla-trabajo").DataTable();
    })
    
    function eliminarPersona(variableEntada,nombrePersona){
    	alert("El indice a eliminar es "+nombrePersona);
    	
    	var jsonSend={
    		'IdPersona':variableEntada,
    		'NombrePersona':nombrePersona
    	}
    	
    	$.ajax({
    		type:"POST",
    		url:"eliminarPersona.do",
    		//data: "IdPersona="+variableEntada+"&Nombre="
    		data:jsonSend,
    		
    		success:function(obj){
    			alert(obj)
    			location.reload();
    			
    		}
    	})
    	
    	
    	
    }


</script>

</html>
